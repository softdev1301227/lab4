package com.chaiwat.lab4;

public class Board {
    private  char[][] board = {{'-', '-', '-'},
                                {'-', '-', '-'},
                                {'-', '-', '-'},};

    private Player player1,player2,currentPlayer;

    public Board(Player player1, Player player2){
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }
    
     public char[][] getBoard() {
        return board;
    }
     
     public Player getCurrentPlayer() {
        return currentPlayer;
    }
     
     public boolean setRowCol(int row , int col){
         if(board [row-1][col-1] == '-'){
             board [row-1][col-1] = currentPlayer.getSymbol();
             return true;
         }
         return false;
     }
     
     public boolean checkWiner(){
         if(checkRow() || checkCol() || checkCross()) {
            saveWin();
            return true;
        }
         return false;
     }
     
     private boolean checkRow(){
         for (int row = 0; row < 3; row++) {
            if (board[row][0] == currentPlayer.getSymbol() && board[row][1] == currentPlayer.getSymbol() && board[row][2] == currentPlayer.getSymbol()) {
                return true;
            }
        }
        return false;
     }
     
     private boolean checkCol(){
          for (int col = 0; col < 3; col++) {
            if (board[0][col] == currentPlayer.getSymbol() && board[1][col] == currentPlayer.getSymbol() && board[2][col] == currentPlayer.getSymbol()) {
                return true;
            }
        }
        return false;
     }
     
     private boolean checkCross(){
        if(board[0][0] == currentPlayer.getSymbol() && board[1][1] == currentPlayer.getSymbol() && board[2][2] == currentPlayer.getSymbol()){
            return true;
        }
        else if (board[0][2] == currentPlayer.getSymbol() && board[1][1] == currentPlayer.getSymbol() && board[2][0] == currentPlayer.getSymbol()){
            return true;
        }
        return false;
     }
     
     public boolean checkDraw(){
        for(int row = 0; row < 3 ; row++){
            for(int col = 0; col < 3; col++){
                if(board[row][col] != 'X' && board[row][col] != 'O'){
                    return false;
                }
            }
        }
        return true;
     }
     
     private void saveWin(){
         if(currentPlayer == player1){
             player1.win();
             player2.lose();
         } else {
             player2.win();
             player1.lose();
         }
     }
     
     void switchPlayer(){
         if(currentPlayer == player1){
             currentPlayer = player2;
         } else {
             currentPlayer = player1;
         }
     }
     
     public void resetBoard() {
         for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }
     }
}
