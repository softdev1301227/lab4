package com.chaiwat.lab4;

import java.util.Scanner;

public class Game {

    private Player player1, player2;
    private Board board;

    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }

    public void Play() {
        boolean isFinish= false;
        newGame();
        while (!isFinish) {
            printBoard();
            printTurn();
            inputRowCol();
            if (board.checkWiner()) {
                printBoard();
                printWinner();
                printPlayers();
                if(wantPlayagin()){
                    board.resetBoard();
                    continue;
                } 
                isFinish= true;
            }
            if (board.checkDraw()) {
                printBoard();
                printDraw();
                printPlayers();
                if(wantPlayagin()){
                    board.resetBoard();
                    continue;
                } 
                isFinish= true;
            }
            board.switchPlayer();
        }
        
    }
    
    private void printBoard() {
        char[][] b = board.getBoard();
         for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(b[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private void printTurn() {
        System.out.println(board.getCurrentPlayer().getSymbol() + " : turn");
    }
    
    private void newGame() {
       board = new Board(player1,player2);
    }
    
    private void printWinner() {
        System.out.println(board.getCurrentPlayer().getSymbol() + " ");
    }
    
    private void printDraw() {
        System.out.println("Draw!!!");
    }
    
    private void printPlayers() {
        System.out.println(player1);
        System.out.println(player2);
    }
    
    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input row,col: ");
        int row = sc.nextInt();
        int col = sc.nextInt();
        board.setRowCol(row, col);

    }
    
    private boolean wantPlayagin() {
        String answer;
        System.out.println("Play again? (y/n)");
        Scanner sc = new Scanner(System.in);
        answer = sc.next();
        if(answer.equals("y")){
            return true;
        }else if(answer.equals("n")){
            return false;
        }
        return false;
    }
}
